extends Control

var game_manager


# Called when the node enters the scene tree for the first time.
func _ready():
	game_manager = get_parent()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_play_button_pressed():
	game_manager.start_game()
