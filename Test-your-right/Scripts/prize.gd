extends Control

@onready var timer : Timer = $Timer
@onready var image : TextureRect = $TextureRect

var game_manager

# Called when the node enters the scene tree for the first time.
func _ready():
	game_manager = get_parent()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func show_image():
	pass


func _on_timer_timeout():
	game_manager.load_thx()


func _on_audio_stream_player_finished():
	image.show()
	timer.start()
