extends Control

@export var player : Node3D
@export var timer : Timer

@onready var right_hand_bar : ProgressBar = $right_hand_progress
@onready var left_hand_bar : ProgressBar = $left_hand_progress
@onready var start_counter : Label = $Start_counter
@onready var time_display : Label = $time_display

var countdown 
var round_time

@onready var strength_required_right = [$"right_hand_progress/25", $"right_hand_progress/40", $"right_hand_progress/55", $"right_hand_progress/70",$"right_hand_progress/100"]
@onready var strength_required_left = [$"left_hand_progress/25",$"left_hand_progress/40",$"left_hand_progress/55",$"left_hand_progress/70",$"left_hand_progress/100"]


# Called when the node enters the scene tree for the first time.
func _ready():
	round_time = Global.set_round_time()
	countdown = 3
	Global.playing = false
	strength_required_right[Global.level -1].show()
	strength_required_left[Global.level -1].show()
	


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	right_hand_bar.value = player.charge_meter_right
	left_hand_bar.value = player.charge_meter_left
	start_counter.text = str(countdown)
	time_display.text = str(round_time)


func _on_timer_timeout():
	if Global.playing == false:
		countdown -= 1
		
		if countdown <= 0:
			Global.playing = true
			start_counter.hide()
		
	else:
		
		if round_time <= 0:
			Global.playing = false
			Global.result = Global.game_result.LOOSE
			player.kill()
		else:
			round_time -= 1		
		
