extends Control

var texts = ["level 1", "level 2", "level 3", "level 4", "level 5"]

@onready var image : TextureRect = $TextureRect
@onready var label : Label = $Label

var game_manager

var current_level 

# Called when the node enters the scene tree for the first time.
func _ready():
	game_manager = get_parent()
	current_level = Global.level -1
	label.text = texts[current_level]


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_timer_timeout():
	game_manager.load_level(current_level)
