extends Node

@onready var timer : Timer = $Timer
@onready var audio_player : AudioStreamPlayer = $AudioStreamPlayer

var levels = preload("res://Nodes/test.tscn")
var menu = preload("res://Nodes/menu.tscn")
var interlude = preload("res://Nodes/level_interlude.tscn")
var retry = preload("res://Nodes/retry.tscn")
var congratulations = preload("res://Nodes/prize.tscn")
var end = preload("res://Nodes/thx.tscn")

var instance

var levels_stream = [load("res://Audio/level1.wav"),load("res://Audio/level2.wav"),load("res://Audio/level3.wav"),load("res://Audio/level4.wav"),load("res://Audio/finallevel.wav")]
var title_audio = load("res://Audio/title.wav")
var loose_audio = load("res://Audio/youloose.wav")
var win_auido = load("res://Audio/youwin.wav")
var countdown_audio = load("res://Audio/cd.wav")
var music = load("res://Audio/music.wav")
var jeers = load("res://Audio/678537__mglennsound__af-crowd-boo-hiss-loop-2.wav")
var congratulations_audio = load("res://Audio/congratulations.wav")
var thanks_for_playing_audio = load("res://Audio/thanks.wav")

# Called when the node enters the scene tree for the first time.
func _ready():
	instance = menu.instantiate()
	add_child(instance)
	play_sound(title_audio)
	


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func start_game():
	remove_child(instance)
	instance = interlude.instantiate()
	add_child(instance)
	play_sound(levels_stream[Global.level -1])
	audio_player.volume_db = 12

func load_congratulations():
	remove_child(instance)
	instance = congratulations.instantiate()
	add_child(instance)
	
func load_thx():
	remove_child(instance)
	instance = end.instantiate()
	add_child(instance)	
	
func load_level(index):
	remove_child(instance)
	instance = levels.instantiate()
	add_child(instance)
	play_sound(countdown_audio)	

func start_timer():
	timer.start()
	
func reset():
	remove_child(instance)
	instance = retry.instantiate()
	add_child(instance)
	play_sound(loose_audio)	

func play_sound(stream):
	audio_player.stream = stream
	audio_player.play()

func play_win_sound():
	audio_player.stream = win_auido
	audio_player.play()	

func play_loose_sound():
	audio_player.stream = jeers
	audio_player.play()	
	
func stop_sound():
	audio_player.stop()	

func _on_timer_timeout():
	if Global.result == Global.game_result.WIN:
		if Global.level < 5:
			Global.level += 1
			start_game()
		else:
			load_congratulations()	
	elif Global.result == Global.game_result.LOOSE:
		reset()
		
	timer.stop()
		


func _on_audio_stream_player_finished():
	if audio_player.stream == countdown_audio:
		audio_player.stream = music
		audio_player.play()
		audio_player.volume_db = -5
