extends Node3D

var damage_array = [25, 40, 55, 70, 95]

@onready var props_array = [$Chulengo,$PC,$Iceblock,$Burger,$Marble]

var damage_required_to_destroy

var player


# Called when the node enters the scene tree for the first time.
func _ready():
	player = get_parent().get_node("Player")
	
	damage_required_to_destroy = damage_array[Global.level - 1]
	props_array[Global.level-1].show()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func recieve_damage(damage):
	if damage >= damage_required_to_destroy:
		player.celebrate()
		Global.result = Global.game_result.WIN
		get_parent().remove_child(self)
	else:
		player.kill()
		Global.result = Global.game_result.LOOSE	
		


