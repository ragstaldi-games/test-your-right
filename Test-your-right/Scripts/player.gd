extends Node3D

const MAX_METER_VALUE = 100.0

@onready var skeleton : Skeleton3D = $Fighter/Armature/Skeleton3D
@onready var hips : PhysicalBone3D = $"Fighter/Armature/Skeleton3D/Physical Bone Pelvis"
@onready var right_ik : SkeletonIK3D = $Fighter/Armature/Skeleton3D/Right_arm_ik
@onready var left_ik : SkeletonIK3D = $Fighter/Armature/Skeleton3D/Left_arm_ik
@onready var anim : AnimationPlayer = $Anim
@onready var audio_player : AudioStreamPlayer3D = $AudioStreamPlayer3D
@onready var eyebrows : MeshInstance3D = $Fighter/Armature/Skeleton3D/Eyebrows
@export var destructible_object : Node3D


var charge_meter_right = 0.0
var charge_meter_left = 0.0

var damage = 0.0

var streams = [load("res://Audio/hit1.wav"), load("res://Audio/hit2.wav")]

var input_sound = load("res://Audio/130420__mmiron__tun_3(1).wav")

var scream = load("res://Audio/449702__digestcontent__female-scream.wav")

var attacked = false

var force = 500

var can_scream = false

# Called when the node enters the scene tree for the first time.
func _ready():
	right_ik.start()
	left_ik.start()
	
	if Global.right_arm_integrity <= 0:
		anim.play("Broken_arm_r")
		
	if Global.left_arm_integrity <= 0:
		anim.play("Broken_arm_l")	
	


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if charge_meter_right > MAX_METER_VALUE:
		charge_meter_right = MAX_METER_VALUE
		
	if charge_meter_left > MAX_METER_VALUE:
		charge_meter_left = MAX_METER_VALUE
	
	
		
		

func _physics_process(delta):
	pass


func _input(event):
	if attacked == false && Global.playing == true:
		audio_player.stream = input_sound
		audio_player.play()
		if event.is_action_pressed("right") && Global.right_arm_integrity > 0:
			charge_meter_right += 1.0			
		
		if event.is_action_pressed("left") && Global.left_arm_integrity > 0:
			charge_meter_left += 2.0				

		if event.is_action_pressed("click"):
			attack()

func attack():
	attacked = true
	get_tree().root.get_node("Game manager").stop_sound()
	get_parent().get_node("ui").get_child(4).stop()
	var random_number = randi() % 2
	audio_player.stream = streams[random_number]
	audio_player.play()
	if charge_meter_right > charge_meter_left || charge_meter_right == charge_meter_left:
		anim.play("Hit")
		damage = charge_meter_right
		Global.right_arm_integrity -= 45
		if Global.right_arm_integrity <= 0:
			can_scream = true
		else:
			can_scream = false		
	else:
		anim.play("Hit_l")		
		damage = charge_meter_left
		Global.left_arm_integrity -= 25
		if Global.left_arm_integrity <= 0:
			can_scream = true
		else:
			can_scream = false	
		
	
		
	
		
		
func kill():
	skeleton.physical_bones_start_simulation()
	hips.apply_impulse(Vector3(0,0,-1) * force)
	Global.right_arm_integrity += 45
	Global.right_arm_integrity += 25
	get_tree().root.get_node("Game manager").play_loose_sound()
	get_tree().root.get_node("Game manager").start_timer()
	

func celebrate():
	if can_scream == false:
		get_tree().root.get_node("Game manager").play_win_sound()
		if Global.right_arm_integrity > 0 && Global.left_arm_integrity > 0:	
			anim.play("Dancew")
		elif Global.right_arm_integrity <=0 && Global.left_arm_integrity > 0:
			anim.play("Dance_only_left")
		elif Global.right_arm_integrity >0 && Global.left_arm_integrity <= 0:
			anim.play("Dance_only_right")		
	
	get_tree().root.get_node("Game manager").start_timer()

func _on_timer_timeout():
	if attacked == false:
		if charge_meter_right > 0:
			charge_meter_right -= 0.1
			
		
		if charge_meter_left > 0:
			charge_meter_left -= 0.1
			




func _on_right_hit_animation_finished(anim_name):
	if anim_name == "Hit" || anim_name == "Hit_l":
		destructible_object.recieve_damage(damage)


func _on_audio_stream_player_3d_finished():
	if (audio_player.stream == streams[0] || audio_player.stream == streams[1]) && can_scream == true:
		eyebrows.hide()	
		audio_player.stream = scream
		audio_player.play()
