extends Node

enum game_result{
	DECIDING = 0,
	WIN = 1,
	LOOSE = 2
}

const LEVELS = 5

var level = 1

var playing = false

var result : game_result = game_result.DECIDING

var round_time_array = [30,25,20,15,10]

var round_time

var right_arm_integrity = 100
var left_arm_integrity = 50


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func set_round_time() -> int:
	round_time = round_time_array[level-1]
	return round_time	

func change_level():
	if level < LEVELS:
		level += 1
		result = game_result.DECIDING
		
